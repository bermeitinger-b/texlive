#!/usr/bin/env bash

#####
#
# * If `~/data` exists, we assume it's mounted from outside
# * For read/write access, we adjust the local user to be the same as the outer one.
#
#####

REMOTE_UID="${REMOTE_UID:-${USER_ID}}"
REMOTE_GID="${REMOTE_GID:-${USER_ID}}"

function fix() {

    local actualUID="${1}"
    local actualGID="${2}"

    groupmod \
        --gid "${actualGID}" \
        "${USER_NAME}"

    usermod \
        --uid "${actualUID}" \
        --gid "${actualGID}" \
        "${USER_NAME}"

    find "${USER_HOME}" \
        ! \( \
        -group "${actualGID}" \
        -a -perm -g+rwX \
        \) \
        -exec chgrp "${actualGID}" -- {} \+ \
        -exec chmod g+rwX -- {} \+

    find "${USER_HOME}" \
        \( \
        -type d \
        -a ! -perm -6000 \
        \) \
        -exec chmod +6000 -- {} \+

}

if [[ "${REMOTE_UID}" != "${USER_ID}" ]]; then

    # Outside user is different from the local user, so we have to
    # change it inside.

    fix "${REMOTE_UID}" "${REMOTE_GID}"

elif test -d "${USER_HOME}/data"; then

    # Outside user is the same (REMOTE_UID was not set, however,
    # the ~/data/ folder is mounted by someone else: we have to change
    # the user to have access

    actualUID="$(stat -c "%u" "${USER_HOME}/data")"
    actualGID="$(stat -c "%g" "${USER_HOME}/data")"

    fix "${actualUID}" "${actualGID}"
fi

exec gosu "${USER_NAME}" /usr/local/bin/start.sh "$@"
