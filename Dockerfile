# syntax=docker/dockerfile:1.4
ARG SCHEME=full
FROM registry.gitlab.com/islandoftex/images/texlive:latest-${SCHEME}
SHELL ["/bin/bash", "-e", "-u", "-o", "pipefail", "-c"]

ENV DEBIAN_FRONTEND="noninteractive"

ENV TERM="dumb"
ENV TEXMFCACHE="/home/tex/texmfcache"

RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF

  # Install runtime dependencies
  apt-get update -q
  apt-get install --yes --no-install-recommends \
    "ca-certificates" \
    "curl" \
    "dumb-init" \
    "gosu" \
    "imagemagick" \
    "locales" \
    "wget" \
    "zip"

  # English default
  echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
  locale-gen

  # Done.
EOF

ENV LC_ALL="en_US.UTF-8"
ENV LANG="en_US.UTF-8"

ENV USER_NAME="texlive"
ENV USER_ID="1000"
ENV USER_HOME="/home/${USER_NAME}"

RUN \
  <<EOF

  # Create empty target folder
  gosu "${USER_NAME}" mkdir "${USER_HOME}/data"

  # Generate caches for user (if exists)
  if command -v luaotfload-tool &> /dev/null; then
    gosu "${USER_NAME}" luaotfload-tool --update
  fi
  if command -v mtxrun &> /dev/null; then
    mtxrun --generate
    texlua /usr/bin/mtxrun.lua --luatex --generate
  fi
  if command -v context &> /dev/null; then
    context --make
    context --luatex --make
  fi

  # Done.
EOF

WORKDIR "${USER_HOME}/data"

COPY start.sh /usr/local/bin/
COPY entrypoint.sh /usr/local/bin/
ENTRYPOINT ["/usr/bin/dumb-init", "/bin/bash", "/usr/local/bin/entrypoint.sh"]

