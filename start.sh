#!/usr/bin/env bash
# Copyright (c) Bernhard Bermeitinger
echo "Welcome to TeXLive 2024"

if [ $# -eq 0 ]; then
    cmd=("bash")
else
    cmd=("$@")
fi

exec "${cmd[@]}"
