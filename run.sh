#!/usr/bin/env bash

IMAGE="registry.gitlab.com/ds-unisg/servers/texlive:full"

exec docker run \
    --rm \
    -it \
    --memory 4g \
    --memory-swap 4g \
    --net=none \
    -e LANG="${LANG}" \
    -v "$(pwd)":/home/texlive/data \
    "${IMAGE}" \
    "$@"
