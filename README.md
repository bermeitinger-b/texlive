# docker-latex

Docker-Latex image for building without the hassle of installing TeXLive.

The base image is from [Island of TeX](https://gitlab.com/islandoftex/images/texlive)

There are **multiple** versions available:

- _scheme-minimal_: `:minimal`
- _scheme-basic_: `:basic`
- _scheme-small_: `:small`
- _scheme-medium_: `:medium`
- _scheme-full_: `:full`
